# Tests that the Chebyshev differentiation matrix works when differentiating functions
import numpy as np

def testeq(error, name):
    """
    testeq(M, N, name)

    Test for equality of two matrices (M and N) allowing for some floating point
    error. The parameter name is the name of the test.
    """
    D = all(error == True)
    if D == True:
        print("Passed " + name)
    else:
        print("Failed " + name)


def runtests_cheb_diff(cheb):
    """
    runtests_cheb(cheb)

    Run a small suite of tests on the Chebyshev differentiation matrix code
    provided. E.g.,

        from chebdiftests import runtests_cheb
        runtests2_cheb(mychebcode)
    """
    #sin test
    x, D = cheb(40)
    u = np.sin(x)
    du = D@u
    du2 = np.cos(x)
    error = np.isclose(du, du2)
    testeq(error, "sine test")
    

    # polynomial test
    x, D = cheb(40)
    u = x**2 + 2*x
    du = D@u
    du2 = 2*x + 2
    error = np.isclose(du, du2)
    testeq(error, "polynomial test")