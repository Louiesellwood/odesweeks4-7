import numpy as np
import pylab
from scipy.integrate import odeint
from scipy.optimize import fsolve

'''first order ODE'''
def dx_dt(x, t, p):
    """
    First order ODE, x is the independent variable, t is the dependent variable and p are the parameters

    Return the right-hand-side of the ODE

        x' = sin(pi*t) - x
    """
    return np.sin(np.pi*t) - x

'''duffing equation'''
def f_duffing(U, t, p):
    '''packaging the dufing eqn into two 1st order form ODEs.'''
    '''returns the derivatives du/dt and d2x/dt2'''
    ''' parameters '''
    omega = p[2]
    tau = p[1]
    zeta = p[0]
    '''state variables'''
    u, du = U
    
    ''' left hand side ''' 
    d2u = tau*np.sin(omega*t) - 2*zeta*du - u - u**3
    '''packaging up the result'''
    dU = [du, d2u]
    return dU

def massspringdamper(x, t, p):
    """
    massspringdamper(x, t, pars)

    Return the right-hand side of the mass-spring-damper differential equation
    in first-order form. The equation is given by

        x'' + 2*xi*x' + x = Gamma*sin(omega*t)

    Parameters to the function
        x
            State variables [x1, x2]
        t
            Time
        pars
            System parameters [xi, Gamma, omega]
    """
    return [x[1], p[1]*np.sin(p[2]*t) - 2*p[0]*x[1] - x[0]]

def solve_ode(f,U,t,p):
    ''' general ode solver'''
    #Using odeint to numerically intergrate the ode (its simple and accurate)
    #It is a fully established library and is a collection of advanced numerical algorithm, 
    #perfect for solving initial-value problems of ordinary differential equations.
    U = odeint(f,U,t,args=(p,))
    ans = [U,t]
    return ans

'''periodic boundary equation (x(0) = x(T))'''
def g(x0,f,T,p):
    '''Rearranging x(0) = x(T) to x(0) - x(T) = 0
    where x(0) is x0 and is our guess to the initial condition,
    f is the ode, T is the period of the ode and p is a list of 
    parameters that are needed to be passed to the ode.'''
    
    xT = odeint(f,x0,[0, T],args=(p,))[-1, :]
    #we want the last element, which will give us x(T)
    return (x0 - xT)
    
'''Uses fsolve to find a root (zero) of a system of nonlinear equations.'''
def root_finding(f,x0,T,p):
    '''Uses numpys built in function fsolve as a root finding method to evaluate the correct x that solves
    the equation returned by g. Again, f is th ode, x0 is the guess to the initial condition, T is the period
    of the ode and p is a list of parameters that are needed to be passed to the ode.'''
    
    realx, info, ier, mesg = fsolve(g, x0, args=(f, T, p), full_output=True)
    
    '''If fsolve doesn't coverge, an error message will be outputed.'''
    message = "No solution found, please try another input value. "
    if ier == 1:
        return realx
    else:
        return message + mesg
    
if __name__ == "__main__":
    # edit the parameter vector, p, to pass to root_finding to include parameters of general ode's
    '''system parameters for duffing equation'''
    w= 1.2    
    T = (2*np.pi)/w
    zeta = 0.05
    tau = 0.2
    p = [zeta,tau,w]
    uguess = [0,1]
    
    '''System parameters for damping equ [xi, Gamma, omega] '''
    #w= 1.2    
    #T = (2*np.pi)/w
    #xi = 0.05
    #Gamma = 0.2
    #p = [xi,Gamma,w]
    #uguess = [0,1]
    
    '''system of parameters for the first order ode'''
#    p = []
#    T = 2
#    uguess = [0]
#    
    #Use guess to find actual intial conditions using our root finder
    '''to test with other ode's, just change the f_duffing to the ode'''
    ureal = root_finding(f_duffing, uguess, T, p)
    
    t = np.linspace(0,T,200)
    u_state,t = solve_ode(f_duffing,ureal,t,p)
    
    '''plotting the result'''
    #tests if there is 1 or 2 state variables (if the ode is of order 2 or 1)
    if(np.shape(u_state) == () or np.shape(u_state) == (200,1)):
        u = u_state
        pylab.plot(t,u, '-b', label = 'position, u')
        pylab.ylabel('position, x')
    else:
        u = u_state[:,0]
        du = u_state[:,1]
        pylab.plot(t,u, '-b', label = 'position, u')
        pylab.plot(t,du, '-r', label = 'velocity, du/dt')
        
    pylab.legend(loc='lower left')
    pylab.xlabel('Time, t')

    