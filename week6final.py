import numpy as np
import sys
from scipy.integrate import odeint
from scipy.optimize import fsolve
'''import functions from other scripts'''
from week4final import root_finding
from collocation_general import collocation
from standard_continuation import standard_continuation
from ploting import plot

def maximum(equ,x0,p,T,num_dimensions):
    '''calcultes maximum of x for given parameter p
    and returns the time and positioin of the maximum'''
    x0_real = root_finding(equ, x0, T, p)
    t = np.linspace(0,T,100)
    X = odeint(equ,x0_real,t,args=(p,))
    xmax_index = np.argmax(X[:,0])
    if num_dimensions == 2:
        xmax = X[xmax_index].tolist()
    else:
        xmax = X[xmax_index][0]
    tmax = t[xmax_index]
    
    return xmax, tmax

def return_y(x0,x1,p0,p1,num_dimensions):
    ''' returns y given the dimensions
    stops code being repeated'''
    if num_dimensions == 2:
            y = np.array([[x0[0],x0[1],p0],[x1[0],x1[1],p1]])
    elif num_dimensions == 1:
            y = np.array([[x0,p0],[x1,p1]])         
    return y

'''contuniation'''
def check_method(equ,method,x0,p0,p1,num_dimensions):
    '''checks which method is passed to pseudo arclength and
    returns the first two points to begin the iteration'''
    if method == "null":
        #Using fsolve only in basic case
        p_new = p[:-1]
        x_0 = fsolve(equ, x0, args=(p0,p_new))
        x_1 = fsolve(equ, x0, args=(p1,p_new))
        y = np.array([[x_0[0],p0],[x_1[0],p1]])
        
    elif method == "shooting":
        #Using numerical shooting to solve ode
        T = p[0]
        #Using shooting method to find real initial conditions and then finding max via odeint
        x_0 = maximum(equ,x0,p[1:],T,num_dimensions)[0]
        #updating parameter list
        p_1 = p.copy()
        p_1[-1] = p1
        #next point
        x_1 = maximum(equ,x_0,p_1[1:],T,num_dimensions)[0]
        #packaging up first and second points
        y = return_y(x_0,x_1,p0,p1,num_dimensions)
                
    elif method == "collocation":
        T = p[1]
        N = p[0]
        #Using collocation method to find real initial conditions
        if len(p)>1:
            p_adjusted = p[2:]
        else:
            p_adjusted = p
        x = collocation(equ,N,x0,p_adjusted)
        x = x[0]
        x_0 = x[abs(np.argmax(x[0]))]
        #estimating second point
        p_1 = p_adjusted.copy()
        p_1[-1] = p1
        x0 = x_0*x0
        x2 = collocation(equ,N,x0,p_1)
        x2 = x2[0]
        x_1 = x2[abs(np.argmax(x2[0]))]
        y = return_y(x_0,x_1,p0,p1,num_dimensions)
    return y,x_0,x_1

def pseudo_arclength(equ,x0,p0,p1,p_vary,p,index,num_dimensions,**kwargs):
    '''checking which method was passed'''
    method = kwargs['discretisation'] 
    y,x_0,x_1 = check_method(equ,method,x0,p0,p1,num_dimensions)  
    '''initialising first and second points'''
    y0 = y[0]
    y1 = y[1]
    #lists to store the variables
    if num_dimensions == 1:
        resultsX = [x_0,x_1]
    else:
        resultsX = [x_0[0],x_1[0]]
    resultsC = [p0,p1]
    #counter for the number of iterations
    it = 0
    while ((resultsC[-1] < p_vary[-1])and(it < 300)):
        '''iterates pseudo arclength algorithm and appends 
        results to the lists above which are then returned 
        to the continuation function'''
        #calculation secant and estimate of next point
        s = y1-y0
        yhat = y1+s
        #updates the parameters list with the addition of the next k value    
        next_p = p1+((it+1)*(p_vary[2]-p_vary[1]))
        p_new = p.copy()
        p_new[-1] = next_p
        #alters yhat so it satifies the conditions of the pseudo arclength condition
        y2 = fsolve(pseudo_condition,yhat,args=(equ,yhat,s,p_new,num_dimensions,method,x0))
        #append results to X and C vectors to return to previous function
        resultsX.append(y2[0])
        resultsC.append(y2[-1])
        #update variables
        y0 = y1
        y1 = y2
        it = it+1
    return resultsX,resultsC
    
def pseudo_condition(y,equ,yhat,s,p,num_of_dimensions,discretisation,x0):
    '''condition in which fsolve with use to return values that satifsfy the condition'''
    x = y[0:-1]
    c = y[-1]
    #different methods
    if discretisation == "shooting":
        T = p[0]
        t = maximum(equ,x0,p[1:],T,num_of_dimensions)[1]
        if num_of_dimensions == 1:
            pos_1 = equ(x,t,p[1:])
            pos = pos_1[0]
        else:
            pos = equ(x,t,p[1:])
    elif discretisation == "collocation":
        N = p[0]
        if num_of_dimensions == 1:
            xx = x*(np.ones((N+1),))
            x2 = collocation(equ,N,xx,p[2:])
            x2 = x2[0]
        elif num_of_dimensions == 2:
            T = p[1]
            xx = x*(np.ones(((N+1),2)))
            x2 = collocation(equ,N,xx,p[2:])
            x2 = x2[0]
        pos = x2[np.argmax(x2[0])]
        '''couldn't get collocation to work correctly:
            pos should be equ(x,t_max,p) where t_max is
            t[np.argmax(x2[0])]'''
    else:
        #if no ODE is passed (no discretisation is needed)
        pos = equ(x,c,p)
        
    if num_of_dimensions == 1:   
        return [pos, np.dot(s,y-yhat)]
    else:
        return [pos[0],pos[1],np.dot(s,y-yhat)]
    
    
def continuation(equ,x0,parameters,index_of_varying_p,step_size,max_steps,**kwargs):
    '''equ is the equation you want to test against a varying parameter, parameters is a list of parameters of the equation, index_of_varying_p is the 
    index at which the varying parameter is in the parameter input, step_size is the step size between each parameter value, max_steps is the numbr of steps
    you want to take and finally **kwargs is the discretisation you want to use (null,shooting,collocation). See example under main function.'''
    #parameter vector
    p0 = parameters[index_of_varying_p]
    p1 = p0 + step_size
    p_end = p0 + (step_size*max_steps)
    p_vary = np.linspace(p0,p_end,max_steps)
    #for non-ODEs
    if kwargs['discretisation'] == "null":
        #checks if the input is the correct dimensions
        dis = "null"
        N =[]
        num_of_dimensions = test_initialcon(x0,"shoot",N)
        xp, p_vary = pseudo_arclength(equ,x0,p0,p1,p_vary,parameters,
                                      index_of_varying_p,num_of_dimensions,
                                      discretisation = "null")    
    elif kwargs['discretisation'] == 'shooting':
        dis = "shoot"
        N =[]
        num_of_dimensions = test_initialcon(x0,"shoot",N)
        xp, p_vary = pseudo_arclength(equ,x0,p0,p1,p_vary,parameters,
                                      index_of_varying_p,num_of_dimensions,
                                      discretisation = "shooting")
    #if the discretisation passed is collocation (ODEs)
    elif kwargs['discretisation'] == 'collocation':
        dis = "coll"
        N = parameters[0]
        num_of_dimensions = test_initialcon(x0,"coll",N)
        xp, p_vary = pseudo_arclength(equ,x0,p0,p1,p_vary,parameters,
                                      index_of_varying_p,num_of_dimensions,
                                      discretisation = "collocation")
    else:
        print("The discretisation passed is invalid.\n")
        print("Please choose from null, shooting or collocation.")
        print("Null is used for functions with a varying parameter and the shooting and collocation discretisations are for periodic boundary ODEs with one varying parameter.")
        sys.exit(1)
    ''' return vector of results along with the parameter vector'''
    return xp,p_vary,dis
        
def test_initialcon(x0,dis,N):
    '''returns an error if an incorrect sized initial guess is passed'''
    e = "This shape of dimensions is invalid, please use a second order ODE at a maximum."
    if dis == "shoot":
        if (np.shape(x0) == (2,)):
            num_of_dimensions = 2
        elif(np.shape(x0) == (1,)):
            num_of_dimensions = 1
        else:
            num_of_dimensions = e
    elif dis == "coll":
        if(np.shape(x0) == (N+1,)):
            num_of_dimensions = 1
        elif(np.shape(x0) == (N+1,2)):
            num_of_dimensions = 2
        else:
            num_of_dimensions = e
    elif dis == "null":
        if(np.shape(x0) == () or np.shape(x0) == (1,)):
            num_of_dimensions = 1
        else:
            num_of_dimensions = e
    return num_of_dimensions

if __name__ == "__main__":
    '''if you're using the collocation method, make sure you pass N, 
    the order to the polymonial equation used, as the first element 
    of the parameter list passed to the continuation fucntion.
    In this case pass T as the second element, otherwise 
    pass T as the first element. This only refers to the case an ODE
    is passed to the continuation function.'''
    
    ''' different equations to test the continuation function with'''
    
    '''Uncomment for solving cubic equation'''
#    from collocation_functions import f
#    k_start = -2
#    k_end = 2
#    p = [k_start] 
#    x0 = [2]
#    ans = continuation(f,x0,p,0,0.1,35,discretisation = "null")
#    
    '''uncomment for second order ODEs'''
    k_start = 0
    k_end = 20
    
    '''uncomment for solving mass spring damper'''
    T = 2
    '''collocation?'''
#    from collocation_functions import mass_spring_damper
#    N = 20
#    x0 = np.zeros((N+1,2))
#    #make sure N is passed first and T is passed second in the parameters
#    p = [N,T,0.05,k_start]
#    ans = continuation(mass_spring_damper,x0,p,3,0.1,100,discretisation = "collocation")
    '''shooting?'''
    from shooting_functions import mass_spring_damper
    #edit x0 but do not change its variable name
    x0 = [0,1]
    #make sure T is passed first in the parameters
    p = [T,0.05,k_start]
    ans = continuation(mass_spring_damper,x0,p,2,0.1,100,discretisation = "shooting")
    '''what the result should look like - using standard continuation and numerical shooting'''
#    ans = standard_continuation(mass_spring_damper,[0,0],[2,0.05],1,0.2,100,discretisation = "shooting")

    '''uncommet for duffing equation'''
    T = 2
    '''collocation?'''
#    from collocation_functions import f_duffing
#    N = 20
#    x0 = np.ones((N+1,2))
#    p = [N,T,0.05,0.5,1,k_start]
#    ans = continuation(f_duffing,x0,p,5,0.1,100,discretisation = "collocation")
    '''shooting?'''
#    from shooting_functions import f_duffing
#    x0 = [0,1]
#    p = [T,0.05,0.5,1,k_start]
#    ans = continuation(f_duffing,x0,p,4,0.1,200,discretisation = "shooting")
#    
    '''uncomment for first order ODE'''
    T = 1
    '''collocation?'''
#    from collocation_functions import dx_dt
#    N=20
#    x0 = np.zeros((N+1,))
#    print(np.shape(x0))
#    k_start = 0
#   #ensure T is second
#    p = [N,T,k_start]
#    ans = continuation(dx_dt,x0,p,2,0.1,100,discretisation = "collocation")
    '''shooting?'''
#    from shooting_functions import dx_dt
#    x0 = [0]
#    p = [T,k_start]
#    ans = continuation(dx_dt,x0,p,1,0.1,60,discretisation = "shooting")
    
    ''' ploting results'''
    plot(ans[0],ans[1],ans[2])