import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
#0.5≤ ω ≤1.5


def f_duffing(U, t, omega):
    '''packaging the dufing eqn in 1st order form.'''
    '''returns the derivatives du/dt and d2x/dt2'''
    zeta = 0.05
    tau = 0.2
    u, du = U
    d2u = tau*np.sin(omega*t) - 2*zeta*du - u - u**3
    dU = [du, d2u]
    return dU

def solve_duffing(tmax, dt, u0, du0,omega):
    #T = (2*np.pi)/omega
    t = np.arange(0,tmax, dt)
    U0 = [u0, du0]
    U = odeint(f_duffing,U0,t,args=(omega,))
    ans = [U,t]
    return ans


if __name__ == "__main__":
    #ans = solve_duffing(10,0.1,0,0,0.5)
    #u = ans[0]
    #t = ans[1]
    fig = plt.figure()
    ax1 = fig.add_axes([0.1,0.5,0.8,0.4],xticklabels=['u(t)'],ylim=(-1,1))
    ax2 = fig.add_axes([0.1,0.1,0.8,0.4],ylim=(-1,1))
    #ax3 = fig.add_axes([])
    #ax4 = fig.add_Axes([])
    u = []
    t = []
    w = np.linspace(0.5,1.5,4)
    for i in range(len(w)):
        uu,tt = solve_duffing(150,0.1,0,0,w[i])
        u.append(uu)
        t.append(tt)
    
    for i in range(len(w)):
        #plots u
        ax1.plot(t[i],u[i][:,0])
        #plots du
        ax2.plot(t[i],u[i][:,1])
    