import numpy as np
from scipy.integrate import odeint
from scipy.optimize import fsolve
from week4final import root_finding
from collocation_general import collocation
import pylab
import sys

def maximum(equ,x0,p,T):
    t = np.linspace(0,T,100)
    X = odeint(equ,x0,t,args=(p,))
    xmax_index = np.argmax(abs(X[:,0]))
    xmax = X[xmax_index]
    
    return xmax 

'''contuniation'''
def pseudo_arclength(equ,x0,p0,p1,p_vary,p,index,num_dimensions,**kwargs):
    '''checking which method was passed'''
    if(kwargs['discretisation'] == "null"):
        discretisation = "null"
        #Using fsolve only in basic case
        p_new = p[:-1]
        x_0 = fsolve(equ, x0, args=(p0,p_new))
        x_1 = fsolve(equ, x0, args=(p1,p_new))
        y = np.array([[x_0[0],p0],[x_1[0],p1]])
        
    elif(kwargs['discretisation'] == "shooting"):
        discretisation = "shooting"
        #Using numerical shooting to solve ode
        T = p[0]
        #Using shooting method to find real initial conditions
        x_initial_real = root_finding(equ, x0, T, p[1:])
        x_0 = maximum(equ,x_initial_real,p[1:],T) 
        #updating parameter list
        p_1 = p.copy()
        p_1[-1] = p1
        #next point
        x_1 = maximum(equ,x0,p_1[1:],T)
        #dimensions of points
        if num_dimensions == 2:
            y = np.array([[x_0[0],x_0[1],p0],[x_1[0],x_1[1],p1]])            
        elif num_dimensions == 1:
            y = np.array([[x_0[0],p0],[x_1[0],p1]])
        
    elif(kwargs['discretisation'] == "collocation"):
        discretisation = "collocation"
        T = p[1]
        N = p[0]
        #Using collocation method to find real initial conditions
        if len(p)>1:
            p_adjusted = p[2:]
        else:
            p_adjusted = p
        #print(p_adjusted)
        x = collocation(equ,N,x0,p_adjusted)
        x = x[0]
        x_0 = x[abs(np.argmax(x[0]))]
        #estimating second point
        p_1 = p_adjusted.copy()
        p_1[-1] = p1
        x0 = x_0*x0
        x2 = collocation(equ,N,x0,p_1)
        x2 = x2[0]
        x_1 = x2[abs(np.argmax(x2[0]))]
        
        if num_dimensions == 2:
            y = np.array([[x_0[0],x_0[1],p0],[x_1[0],x_1[1],p1]])
            print(y)
        elif num_dimensions == 1:
            y = np.array([[x_0,p0],[x_1,p1]])
    '''initialising first and second points'''
    y0 = y[0]
    y1 = y[1]
    #lists to store the variables
    resultsX = [x_0,x_1]
    resultsC = [p0,p1]
    #counter for the number of iterations
    it = 0
    while ((resultsC[-1] < p_vary[-1])and(it < 300)):
        #calculation secant and estimate of next point
        if num_dimensions == 1:
            s = y1-y0
            yhat = y1+s
        elif num_dimensions == 2:
            s = [y1[0]-y0[0],y1[1]-y0[1],y1[2]-y0[2]]
            yhat = [y1[0]+s[0],y1[1]+s[1],y1[2]+s[2]]
            
        next_p = p1+((it+1)*(p_vary[2]-p_vary[1]))
        p_new = p.copy()
        p_new[-1] = next_p
            
        y2 = fsolve(pseudo_condition,yhat,args=(equ,yhat,s,p_new,num_dimensions,discretisation))
        #append results to X and C vectors to return to previous function
        if num_dimensions == 1:
            resultsX.append(y2[0])
            resultsC.append(y2[1])
        elif num_dimensions == 2:
            resultsX.append([y2[0],y2[1]])
            resultsC.append(y2[2])
        #update variables
        y0 = y1
        y1 = y2
        it = it+1
        
    return resultsX,resultsC
    
def pseudo_condition(y,equ,yhat,s,p,num_of_dimensions,discretisation):
    if num_of_dimensions == 1:
        x = y[0]
        c = y[1]
        if discretisation == "shooting":
            T = p[0]
            x_real = root_finding(equ, x, T, p[1:])
            pos = x_real[abs(np.argmax(x_real[0]))]
        elif discretisation == "collocation":
            N = p[0]
            xx = x*(np.ones((N+1),))
            x2 = collocation(equ,N,xx,p[2:])
            x2 = x2[0]
            pos = x2[abs(np.argmax(x2[0]))]
        else:
            pos = equ(x,c,p)
        return [pos, np.dot(s,y-yhat)]
    elif num_of_dimensions == 2:
        x = [y[0],y[1]]
        c = y[2]
        if discretisation == "shooting":
            T = p[0]
            print(p)
            x_real = root_finding(equ, x, T, p[1:])
            print(x_real)
            #print(x_initial_real)
            #print(p)
            #pos = x_real[abs(np.argmax(x_real))]
            pos = maximum(equ,x,p[1:],T)
            print(pos)
        elif discretisation == "collocation":
            N = p[0]
            xx = x*(np.ones(((N+1),2)))
            #print(p)
            x2 = collocation(equ,N,xx,p[2:])
            x2 = x2[0]
            pos = x2[abs(np.argmax(x2[0]))]
        return [equ(x,t,p),np.dot(s,y-yhat)]
    
    
def continuation(equ,x0,parameters,index_of_varying_p,step_size,max_steps,**kwargs):
    '''equ is the equation you want to test against a varying parameter, parameters is a list of parameters of the equation, index_of_varying_p is the 
    index at which the varying parameter is in the parameter input, step_size is the step size between each parameter value, max_steps is the numbr of steps
    you want to take and finally **kwargs is the discretisation you want to use (null,shooting,collocation). See example under main function.'''
    #parameter vector
    p0 = parameters[index_of_varying_p]
    p1 = p0 + step_size
    p_end = p0 + (step_size*max_steps)
    p_vary = np.linspace(p0,p_end,max_steps)
    #for non-ODEs
    if kwargs['discretisation'] == "null":
        #checks if the input is the correct dimensions
        N =[]
        num_of_dimensions = test_initialcon(x0,"shoot",N)
        xp, p_vary = pseudo_arclength(equ,x0,p0,p1,p_vary,parameters,
                                      index_of_varying_p,num_of_dimensions,
                                      discretisation = "null")    
    elif kwargs['discretisation'] == 'shooting':
        N =[]
        num_of_dimensions = test_initialcon(x0,"shoot",N)
        xp, p_vary = pseudo_arclength(equ,x0,p0,p1,p_vary,parameters,
                                      index_of_varying_p,num_of_dimensions,
                                      discretisation = "shooting")
    #if the discretisation passed is collocation (ODEs)
    elif kwargs['discretisation'] == 'collocation':
        N = parameters[0]
        num_of_dimensions = test_initialcon(x0,"coll",N)
        xp, p_vary = pseudo_arclength(equ,x0,p0,p1,p_vary,parameters,
                                      index_of_varying_p,num_of_dimensions,
                                      discretisation = "collocation")
    else:
        print("The discretisation passed is invalid.\n")
        print("Please choose from null, shooting or collocation.")
        print("Null is used for functions with a varying parameter and the shooting and collocation discretisations are for periodic boundary ODEs with one varying parameter.")
        sys.exit(1)
    ''' return vector of results along with the parameter vector'''
    return xp,p_vary,num_of_dimensions
        
def test_initialcon(x0,dis,N):
    e = "This shape of dimensions is invalid, please use a second order ODE at a maximum."
    if dis == "shoot":
        if (np.shape(x0) == (2,)):
            num_of_dimensions = 2
        elif(np.shape(x0) == (1,)):
            num_of_dimensions = 1
        else:
            num_of_dimensions = e
    elif dis == "coll":
        if(np.shape(x0) == (N+1,)):
            num_of_dimensions = 1
        elif(np.shape(x0) == (N+1,2)):
            num_of_dimensions = 2
        else:
            num_of_dimensions = e
    elif dis == "null":
        if(np.shape(x0) == () or np.shape(x0) == (1,)):
            num_of_dimensions = 1
        else:
            num_of_dimensions = e
    return num_of_dimensions

if __name__ == "__main__":
    '''if you're using the collocation method, make sure you pass N, 
    the order to the polymonial equation used, as the first element 
    of the parameter list passed to the continuation fucntion.
    In this case pass T as the second element, otherwise 
    pass T as the first element. This only refers to the case an ODE
    is passed to the continuation function.'''
    
    ''' different equations to test the continuation function with'''
    
    '''Uncomment for solving cubic equation'''
    from collocation_functions import f
    k_start = -2
    k_end = 2
    p = [k_start] 
    x0 = [2]
    ans = continuation(f,x0,p,0,0.1,35,discretisation = "null")
   
    '''uncomment for second order ODEs'''
    k_start = 0
    k_end = 20
    
    '''uncomment for solving mass spring damper'''
    T = 2
    '''collocation?'''
#    from collocation_functions import mass_spring_damper
#    N = 20
#    x0 = np.zeros((N+1,2))
#    #make sure N is passed first and T is passed second in the parameters
#    p = [N,T,0.05,k_start]
#    ans = continuation(mass_spring_damper,x0,p,3,0.1,100,discretisation = "collocation")
    '''shooting?'''
#    from shooting_functions import mass_spring_damper
#    #edit x0 but do not change its variable name
#    x0 = [0,-2]
#    #make sure T is passed first in the parameters
#    p = [T,0.05,k_start]
#    ans = continuation(mass_spring_damper,x0,p,2,0.1,100,discretisation = "shooting")
    
    '''uncommet for duffing equation'''
    T = 2
    '''collocation?'''
#    from collocation_functions import f_duffing
#    N = 20
#    x0 = np.ones((N+1,2))
#    p = [N,T,0.05,0.5,1,k_start]
#    ans = continuation(f_duffing,x0,p,5,0.1,60,discretisation = "collocation")
    '''shooting?'''
#    from shooting_functions import f_duffing
#    x0 = [0,10]
#    p = [T,0.05,0.5,1,k_start]
#    ans = continuation(f_duffing,x0,p,4,0.1,60,discretisation = "shooting")
#    
    '''uncomment for first order ODE'''
    T = 1
    '''collocation?'''
#    from collocation_functions import dx_dt
#    N=20
#    x0 = np.zeros((N+1,))
#    print(np.shape(x0))
#    k_start = 0
#    p = [N,T,k_start]
#    ans = continuation(dx_dt,x0,p,2,0.1,100,discretisation = "collocation")
    '''shooting?'''
#    from shooting_functions import dx_dt
#    x0 = [0]
#    p = [T,k_start]
#    ans = continuation(dx_dt,x0,p,1,0.1,60,discretisation = "shooting")
#    
    ''' ploting results'''
    u_state = ans[0]
    p_vary = ans[1]
    
    #tests if there is 1 or 2 state variables (if the ode is of order 2 or 1)
    if ans[2] == 1:
        u = u_state
        pylab.plot(p_vary,u, '-b', label = 'position, u')
        pylab.xlabel('parameter, K')
        pylab.ylabel('x')
        #pylab.xlim((-1,1))
        #pylab.ylim((-1.5,1.5))
        pylab.grid('on','both','both')
    else:
        u, du = map(list, zip(*u_state))
        pylab.plot(p_vary,u, '-b')
        #pylab.plot(p_vary,du, '-r', label = 'velocity, du/dt')
        pylab.legend(loc='upper right')
        pylab.xlabel('parameter, K')
        pylab.ylabel('xmax, |x|')
        pylab.grid('on','both','both')
        pylab.xlim((0,20))