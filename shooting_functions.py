import numpy as np

'''cubic equation'''
def f(x,c,p):
    return x**3-x+c

'''first order ODE'''
def dx_dt(x, t, p):
    """
    First order ODE, x is the independent variable, t is the dependent variable and p are the parameters

    Return the right-hand-side of the ODE

        x' = sin(pi*t) - x
    """
    return np.sin(np.pi*t) - x + p

''' mass spring damper system'''
def mass_spring_damper(x,t,p):
    """
    massspringdamper(x, t, pars)

    Return the right-hand side of the mass-spring-damper differential equation
    in first-order form. The equation is given by

        x'' + 2*xi*x' + k*x = sin(pi*t)

    Parameters to the function
        x
            State variables [x1, x2]
        t
            Time
        p
            System parameters [k, xi]
            
            where, xi = 0.05, k = range from 0.1 to 20
    """
    #print('para')
    #print(p)
    #print(x)
    return [x[1], np.sin(np.pi*t) - 2*p[0]*x[1] - p[1]*x[0]]

'''duffing equation'''
def f_duffing(U,t,p):
    '''packaging the dufing eqn in 1st order form.'''
    '''returns the derivatives du/dt and d2x/dt2'''
    ''' parameters '''
    k = p[3]
    Beta = p[2]
    tau = p[1]
    zeta = p[0]
    '''state variables'''
    u = U[0]
    du = U[1]
    ''' left hand side ''' 
    d2u = tau*np.sin(np.pi*t) - 2*zeta*du - k*u - Beta*u**3
    '''packaging up the result'''
    dU = [du, d2u]
    return dU
