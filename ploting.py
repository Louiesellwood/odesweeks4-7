import pylab

def plot(x,y,dis):
    
    if dis == "null":
        pylab.plot(y,x, '-b', label = 'position, u')
        pylab.ylabel('Variable, x')
    elif dis == "shoot":
        pylab.plot(y,x, '-b', label = 'position, u')
        pylab.ylabel('Maximum Displacement, |x|')
    elif dis == "coll":
        pylab.plot(y,x, '-b', label = 'position, u')
        pylab.ylabel('Maximum Displacement, |x|')
    pylab.xlabel('Parameter, K')
    pylab.grid('on','both','both')