import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from scipy.optimize import fsolve
from collocation_tests import runtests_ode
        

def dy_dx(x, t, p):
    '''x' = sin(wt) - x
    x is our inital condition'''
    return np.sin(np.pi*t) - x

        
def cheb(N):
    if N == 0:
        D = 0
        x = 0
        return x,D
    else:
        x = np.cos(np.pi*(np.arange(0,N+1))/N)
        X = np.tile(x,(N+1,1))
        #X = repmat(x,(1,N+1))
        c = np.vstack((2,np.ones((N-1,1)),2))*(-1)**((np.arange(N+1)).T)
        dX = X-X.T
        D = (c*((1/c).T))/(dX+(np.eye(N+1)))
    
        sumDt = [sum(x) for x in zip(*D.T)]
        D = D - np.diag(sumDt)
        #x goes from 1 to -1
        #in order to increase the range by double, multiply x by 2 and divide D by 2
        #we are returning -D and -x bceause it flips them so it goes from -1 to 1
        
        return -x,D

'''periodic boundary equation (x(0) = x(T))'''
def g(x,f,D,t,p):
    #Rearrange x(0)=x(T) to x(0)-x(T)=0
    #where x0 is our [x(0),dx/dt(0)] and x(0)
    #odeint(...) is our x(T)
    #x0 is our guess to the initial conditions

    #print(np.shape(t))
    df_estimate = D@x
    xx = df_estimate - f(x,t,p)
    #print(np.shape(xx))
    xx[-1] = x[0]-x[-1]
    
    return xx

def root_finding(f,x0,T,p):
    #solve the periodic boundary value problem using scipy's fsolve because it has a very low tollerance and will coverage to an accurate estimate.
    #fsolve will choose Newtons method. Each iteration involves the approximate solution of a large linear system using the method of preconditioned conjugate gradients (PCG).
    realx, info, ier, mesg = fsolve(g, x0, args=(f, T, p), full_output=True)
    
    #if root doesnt converge
    message = "No solution found, please try another input value. "
    if ier == 1:
        return realx
    else:
        return message + mesg

def collocation(ode,N,x0,p):
    #uses fsolve which uses g
    #need to split up the return of g()
    #w, T = p
    t,D = cheb(N)
    
    realx, info, ier, mesg = fsolve(g, x0, args=(ode, D, t, p), full_output=True)
    
    #if root doesnt converge
    message = "No solution found, please try another input value. "
    if ier == 1:
        return realx, t
    else:
        return message + mesg
    
    

if __name__ == "__main__":
    w= 1.2    
    T = (2*np.pi)/w
    N = 20
    p = [w,T]
    #Use guess to find actual intial conditions using our root finder
    xguess = np.zeros((N+1))
    xreal,t = collocation(dy_dx, N, xguess, p)
    
    exactsoln1 = 1/(1+np.pi**2)*np.sin(np.pi*t) - np.pi/(1+np.pi**2)*np.cos(np.pi*t)
    
    runtests_ode(collocation)

#    y = dy_dx(0,tt,p)
    #u,t = solve_duffing(ureal,t,w)
#    u,t = dy_dx(xreal,tt,w)
    plt.plot(t,xreal, '-b')
    plt.plot(t,exactsoln1, '-r')
#    plt.show()