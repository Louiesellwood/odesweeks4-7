import numpy as np
from scipy.integrate import odeint
from scipy.optimize import fsolve
from scipy.optimize import fmin
import pylab

''' different equations to test the continuation function with'''
'''cubic equation'''
def f(x,c):
    return x**3-x+c

''' mass spring damper system'''
def mass_spring_damper(x, t, p):
    """
    massspringdamper(x, t, pars)

    Return the right-hand side of the mass-spring-damper differential equation
    in first-order form. The equation is given by

        x'' + 2*xi*x' + k*x = sin(pi*t)

    Parameters to the function
        x
            State variables [x1, x2]
        t
            Time
        p
            System parameters [xi, k]
            
            where, xi = 0.05, t = range from 0.1 to 2 
    """
    
    return [x[1], np.sin(np.pi*t) - 2*p[0]*x[1] - p[1]*x[0]]

'''duffing equation'''
def f_duffing(U, t, p):
    '''packaging the dufing eqn in 1st order form.'''
    '''returns the derivatives du/dt and d2x/dt2'''
    ''' parameters '''
    k = p[3]
    Beta = p[2]
    tau = p[1]
    zeta = p[0]
    '''state variables'''
    u, du = U
    
    ''' left hand side ''' 
    d2u = tau*np.sin(np.pi*t) - 2*zeta*du - k*u - Beta*u**3
    '''packaging up the result'''
    dU = [du, d2u]
    return dU


def solve_ode(f,U,t,p):
    ''' general ode solver'''
    #Using odeint to numerically intergrate the ode (its simple and accurate)
    #It is a fully established library and is a collection of advanced numerical algorithm, 
    #perfect for solving initial-value problems of ordinary differential equations.
    U = odeint(f,U,t,args=(p,))
    ans = [U,t]
    return ans

''' chebyshev matrix'''

def cheb(N):
    if N == 0:
        D = 0
        x = 0
        return x,D
    else:
        x = np.cos(np.pi*(np.arange(0,N+1))/N)
        X = np.tile(x,(N+1,1))
        #X = repmat(x,(1,N+1))
        c = np.vstack((2,np.ones((N-1,1)),2))*(-1)**((np.arange(N+1)).T)
        dX = X-X.T
        D = (c*((1/c).T))/(dX+(np.eye(N+1)))
        sumDt = [sum(x) for x in zip(*D.T)]
        D = D - np.diag(sumDt)
        return -x,D

'''root finding '''

def g(x0,ode,T,p):
    #Rearrange x(0)=x(T) to x(0)-x(T)=0
    #where x0 is our [x(0),dx/dt(0)] and x(0)
    #odeint(...) is our x(T)
    #x0 is our guess to the initial conditions
    
    xT = odeint(ode,x0,[0, T],args=(p,))[-1, :]
    #we want the last element which will give us x(T)
    return (x0 - xT)

def root_finding(ode,x0,T,p):
    #solve the periodic boundary value problem using scipy's fsolve because it has a very low tollerance and will coverage to an accurate estimate.
    #fsolve will choose Newtons method. Each iteration involves the approximate solution of a large linear system using the method of preconditioned conjugate gradients (PCG).
    realx, info, ier, mesg = fsolve(g, x0, args=(ode, T, p), full_output=True)
    
    #if root doesnt converge
    message = "No solution found, please try another input value. "
    if ier == 1:
        return realx
    else:
        print(message + mesg)

'''contuniation'''
def pseudo_arclength(equ,x0,p0,p1,p_vary):
    x0 = fsolve(equ, x0, args=(p0))
    x1 = fsolve(equ, x0, args=(p1))
    
    y = np.array([[x0[0],p0],[x1[0],p1]])
    y0 = y[0]
    y1 = y[1]
    resultsX = [x0,x1]
    resultsC = [p0,p1]
    
    while resultsC[-1] < p_vary[-1]:
        s = y1-y0
        yhat = y1+s
        y2 = fsolve(pseudo_condition,yhat,args=(yhat,s))
     
        resultsX.append(y2[0])
        resultsC.append(y2[1])
    
        y0 = y1
        y1 = y2
    
    return resultsX,resultsC
    
def pseudo_condition(y,yhat,s):
    x = y[0]
    c = y[1]
    return [f(x,c), np.dot(s,y-yhat)]
    
    
    
def continuation(equ,x0,p_0,p_end,p,**kwargs):
    #if the discretisation passed is null then
    if kwargs['discretisation'] == "null":
        #checks if the input is the correct dimensions
        assert (np.shape(x0) == () or np.shape(x0) == (1,)), "too many or too few initial states: only one initial is needed"
        p_vary = np.linspace(p_0,p_end,100)
        p_0 = p_vary[0]
        p_1 = p_vary[1]
        xp, p_vary = pseudo_arclength(equ,x0,p_0,p_1,p_vary)        
        
    #if the discretisation passed is shooting then        
    elif kwargs['discretisation'] == 'shooting':
        assert (np.shape(x0) == (2,)), "too many or too few initial states: only one initial is needed"
        T = p[len(p)-1]
        p.remove(p[len(p)-1])
        p.append(p_vary[0])
        x0_1 = root_finding(equ,x0, T, p)
        p.remove(p_vary[0])
        xp = []
        
        for i in range(len(p_vary)):
            p.append(p_vary[i])
            x = fsolve(equ,x0_1, args=(T,p))
            xp.append(x)
            p.remove(p_vary[i])
    #if the discretisation passed is collocation then    
    elif kwargs['discretisation'] == 'collocation':
        '''blah blah blah'''
        xp = 'collocation x'
        p_vary = 'collocation p_vary'
    return xp,p_vary
        
def maximum(equ,x0,*p):
    fullsoln = odeint(equ,y2[1],[0, T],args=(p,))
    pylab.plot(y2[0],max(fullsoln))  
    

if __name__ == "__main__":
    ''' solving f '''
#    text = input("What start state do you want to use?") how to ask user stuff
    
    K_start = -2
    k_end = 2
    x0 = [2]
#    #conitnuation(equ,x0,p_vary,p,**kwargs)
    ans = continuation(f,x0, K_start, k_end, [], discretisation = "null")
    
#    K = np.linspace(0.1,20,100)
#    x0 = [0,-2]
    '''solving mass spring damper'''
#    make sure T is last
#    T = 2
#    p = [0.05,T]
#    ans = continuation(mass_spring_damper,x0,K,p,discretisation = "shooting")
    
    '''solving duffing equation'''
#    Beta = p[2]
#    tau = p[1]
#    zeta = p[0]
#    T = 2
#    p = [0.05,0.5,1,T]
#    ans = continuation(f_duffing,x0,K,p,discretisation = "shooting")
    
    ''' ploting results'''
    u_state = ans[0]
    p_vary = ans[1]
    
    #tests if there is 1 or 2 state variables (if the ode is of order 2 or 1)
    if(np.shape(x0) == () or np.shape(x0) == (1,)):
        u = u_state
        pylab.plot(p_vary,u, '-b', label = 'position, u')
    else:
        u, du = map(list, zip(*u_state))
        pylab.plot(p_vary,u, '-b', label = 'position, u')
        pylab.plot(p_vary,du, '-r', label = 'velocity, du/dt')
        
    pylab.legend(loc='upper right')
    pylab.xlabel('parameter, K')
    
'''augmented = @(y, yhat, secant)[ ...
    f(y(1:end-1), y(end)); ... % The original equation
    secant'*(y - yhat)];'''