import numpy as np
import pylab
import sys
from scipy.integrate import odeint
from scipy.optimize import fsolve
#from collocation_tests import runtests_ode
        

def dx_dt(x, t, p):
    """
    First order ODE, x is the independent variable, t is the dependent variable and p are the parameters

    Return the right-hand-side of the ODE

        x' = sin(pi*t) - x
    """
    return np.sin(np.pi*t) - x

def d2x_dt2(x, t, p):
    """
    Second order ODE, x is the independent variable, t is the dependent variable and p are the parameters
    Need two initial states: x = [x, x_dot]

    Return the right-hand-side of the ODE

        x'' + par[0]*x' + par[1]*x = sin(pi*t)
    """
    return [x[:,1], np.sin(np.pi*t) - p[0]*x[:,1] - p[1]*x[:,0]]
        
def cheb(N):
    '''N is the order of the polynomial, x is the '''
    if N == 0:
        D = 0
        x = 0
        return x,D
    else:
        x = np.cos(np.pi*(np.arange(0,N+1))/N)
        X = np.tile(x,(N+1,1))
        c = np.vstack((2,np.ones((N-1,1)),2))*(-1)**((np.arange(N+1)).T)
        dX = X-X.T
        D = (c*((1/c).T))/(dX+(np.eye(N+1)))
    
        sumDt = [sum(x) for x in zip(*D.T)]
        D = D - np.diag(sumDt)
        #x goes from 1 to -1
        #in order to increase the range by double, multiply x by 2 and divide D by 2
        #we are returning D and -x bceause it flips them so it goes from -1 to 1 instead of 
        #1 to -1
        
        return -x,D

'''periodic boundary equation (x(0) = x(T))'''
def g(x,ode,D,t,N,p,num_of_dimensions):
    '''returns both the periodic boundary value condition and the chebyshev estimate to the differentiation of x'''
    #estimates the derivative of the ode passed to collocation
       
    if np.shape(x) == (N+1,):
        x0 = x
    elif(np.shape(x) == (((2*N)+2),)):
        x0 = x.reshape((-1,2))
        assert x0.shape[0] == N+1
    
    df_estimate = D@x0
    
    if num_of_dimensions == 2:
        assert df_estimate.shape[0] == N+1
        print(p)
        x_real = ode(x0,t,p)
        x_real_reshaped = list(map(list, zip(*x_real)))
        #print(x_real)
        print(np.shape(x_real_reshaped))
        #assert type(x_real) == np.ndarray
        
        assert np.shape(x_real_reshaped) == (N+1,num_of_dimensions)
        x_not_reshaped = df_estimate - x_real_reshaped
        #sets last element to the periodic boundary condition
        print(np.shape(x_not_reshaped))
        print(type(x_not_reshaped))
        print(type(x))
        x_not_reshaped[-1] = x[0]-x[-1]
        xx = x_not_reshaped.reshape((-1, ))
        print(np.shape(xx))
    else:
        x_real = ode(x0,t,p)
        xx = df_estimate - x_real
        xx[-1] = x0[0]-x0[-1]
        
    return xx


def collocation(ode,N,x0,p):
    '''
    ode is the right-hand-side of the ODE, N is the order of the
    polynomial to use, x0 is the initial guess at the solution, and p is the
    parameters (if any, use [] for none).
    '''
    '''check's the order of the ode is acceptable'''
    if np.shape(x0) == (N+1,):
        num_of_dimensions = 1
    elif(np.shape(x0) == (N+1,2)):
        num_of_dimensions = 2
    else:
        print('ERROR\n')
        print('Invaild initial condition, this algorithm only works for first and second order ODEs.')
        print('The initial condition should be entered like the following: [0,0]\n')
        sys.exit(1)
        
    t,D = cheb(N)
    
    realx, info, ier, mesg = fsolve(g, x0, args=(ode, D, t, N, p,num_of_dimensions), full_output=True)
    if num_of_dimensions == 1:
        ans = realx
        t_ans = t
    else:
        ans = realx.reshape((-1,2))
        t_ans = t.reshape((-1,1))
    #if root doesnt converge
    message = "No solution found, please try another input value. "
    if ier == 1:
        return ans, t_ans
    else:
        return message + mesg
    
    
if __name__ == "__main__":
    '''parameters to pass to collocation function'''
    '''uncomment for first order ode'''
    '''for dx_dt, no parameters needed'''
#    p = []
#    N = 20
#    xguess = np.zeros((N+1))
#    ans = collocation(dx_dt, N, xguess, p)
    
    '''uncomment for second order ode'''
    w= 1.2    
    T = (2*np.pi)/w
    p = [w,T]
    N = 20
    
    '''initial guess of vector of zeros'''
    xguess = np.zeros((N+1,2))
    '''actual answers from collocation method'''
    ans = collocation(d2x_dt2, N, xguess, p)
    
    
    '''plot results'''
    u_state = ans[0]
    t = ans[1]
    if(np.shape(u_state) == (N+1,)):
        pylab.plot(t,u_state, '-b', label = 'position, x')
        pylab.xlabel('time, t')
        pylab.ylabel('x')
    else:
        #map splits up the output correctly
        u, du = map(list, zip(*u_state))
        pylab.plot(t,u, '-b', label = 'position, x')
        pylab.plot(t,du, '-r', label = 'velocity, dx/dt')
        pylab.legend(loc='upper right')
        pylab.xlabel('time, t')
        pylab.ylabel('x')
    
    
    runtests_ode(collocation)