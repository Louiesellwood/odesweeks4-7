import numpy as np
import matplotlib.pyplot as plt
from chebtests import runtests_cheb
from chebdiftests import runtests_cheb_diff
        
def cheb(N):
    if N == 0:
        D = 0
        x = 0
        return x,D
    else:
        x = np.cos(np.pi*(np.arange(0,N+1))/N)
        X = np.tile(x,(N+1,1))
        #X = repmat(x,(1,N+1))
        c = np.vstack((2,np.ones((N-1,1)),2))*(-1)**((np.arange(N+1)).T)
        dX = X-X.T
        D = (c*((1/c).T))/(dX+(np.eye(N+1)))
    
        sumDt = [sum(x) for x in zip(*D.T)]
        D = D - np.diag(sumDt)
        #x goes from 1 to -1
        #in order to increase the range by double, multiply x by 2 and divide D by 2
        #we are returning -D and -x bceause it flips them so it goes from -1 to 1
        
        return -x,D

#runs tests to check if the differentiation martix passes them    
runtests_cheb(cheb)
#runs tests to check if the differentiation martix differentiates correctly
runtests_cheb_diff(cheb) 

#u' of u = cos(x) is u' = -sin(x)
#u' = [x0',x1',x2',...,xN-1']' = Du = D[x0,x1,x2,...,xN-1]
xreal = np.linspace(-np.pi,np.pi,100)
ureal = np.sin(xreal)
dureal = np.cos(xreal)

x, D = cheb(10)
u = np.sin(x)
du = D@u
du2 = np.cos(x)
print("Sin test: ", np.isclose(du, du2))
plt.plot(x,du, 'bo')
plt.plot(xreal,dureal,'r-')