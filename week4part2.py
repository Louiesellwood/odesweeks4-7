#how to do question 2:
#so we manually take the period for the graph at a specific value of w (OMEGA) and we use that period here.
#Our condition: x(0) = x(T) which we can rearrage to g(x) = x(0) - x(T) = 0
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from scipy.optimize import fsolve

'''duffing equation'''
def f_duffing(U, t, omega):
    '''packaging the dufing eqn in 1st order form.'''
    '''returns the derivatives du/dt and d2x/dt2'''
    zeta = 0.05
    tau = 0.2
    u, du = U
    d2u = tau*np.sin(omega*t) - 2*zeta*du - u - u**3
    dU = [du, d2u]
    return dU

def solve_duffing(U,t,omega):
    #T = (2*np.pi)/omega
    #Using odeint to numerically intergrate the duffing equation (its simple and accurate)
    #It is a fully established library and is a collection of advanced numerical algorithm, 
    #perfect for solving initial-value problems of ordinary differential equations.
    U = odeint(f_duffing,U,t,args=(omega,))
    ans = [U,t]
    return ans

'''periodic boundary equation (x(0) = x(T))'''
def g(x0,f,T,p):
    #Rearrange x(0)=x(T) to x(0)-x(T)=0
    #where x0 is our [x(0),dx/dt(0)] and x(0)
    #odeint(...) is our x(T)
    #x0 is our guess to the initial conditions
    
    xT = odeint(f,x0,[0, T],args=(p,))[-1, :]
    #we want the last element which will give us x(T)
    return (x0 - xT)
    
#now use root finding method to test if a guess of a xvalue passed into g(x) returns 0, if so the x value is the correct initial condition.
'''Uses fsolve to find a root (zero) of a system of nonlinear equations.'''
def root_finding(f,x0,T,p):
    #solve the periodic boundary value problem using scipy's fsolve because it has a very low tollerance and will coverage to an accurate estimate.
    #fsolve will choose Newtons method. Each iteration involves the approximate solution of a large linear system using the method of preconditioned conjugate gradients (PCG).
    realx, info, ier, mesg = fsolve(g, x0, args=(f, T, p), full_output=True)
    
    #if root doesnt converge
    message = "No solution found, please try another input value. "
    if ier == 1:
        return realx
    else:
        return message + mesg
    
if __name__ == "__main__":
    w= 1.2    
    T = (2*np.pi)/w
    
    #Use guess to find actual intial conditions using our root finder
    uguess = [0,1]
    ureal = root_finding(f_duffing, uguess, T, w)
    
    t = np.linspace(0,T,200)
    u,t = solve_duffing(ureal,t,w)
    
    plt.plot(t,u)
    plt.show()
    