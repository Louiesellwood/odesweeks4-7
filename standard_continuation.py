from shooting_functions import mass_spring_damper
from shooting_functions import f_duffing
from week4final import root_finding
from week6part2 import continuation
from scipy.optimize import fsolve
from scipy.integrate import odeint
import numpy as np
import pylab

k_start = -2
k_end = 2
#p = [0.05]
#x0 = [2]
#ans = continuation(mass_spring_damper,x0,p,0,0.1,35,discretisation = "null")
def standard_continuation(equ,x0,parameters,index_of_varying_p,step_size,max_steps,**kwargs):
    p0 = parameters[index_of_varying_p]
    #p1 = p0 + step_size
    p_end = p0 + (step_size*max_steps)
    k = np.linspace(p0,p_end,100)
    t = np.linspace(0,parameters[0],100)
#x = [0,1]
    y = []
    for i in range(len(k)):
        p_new = parameters.copy()
        p_new.append(k[i])
        x_r = root_finding(equ,x0,parameters[0],p_new[1:])
        y1 = odeint(equ,x_r,t,args=(p_new[1:],))[0]
        y.append(abs(y1[0]))
    return y,k
    
y,k = standard_continuation(f_duffing,[0,0],[2,0.05,0.5,1,0],4,0.1,200,discretisation = "shooting")
pylab.plot(k,y)
pylab.xlabel('parameter, K')
pylab.ylabel('x')
pylab.grid('on','both','both')